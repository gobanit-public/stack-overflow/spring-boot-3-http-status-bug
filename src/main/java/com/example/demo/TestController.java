package com.example.demo;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@RequestMapping("test")
public class TestController {


    @GetMapping(value = "/ok")
    public String helloOk() {
        return "hello";
    }

    @GetMapping(value = "/fail")
    public String helloFail() {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "test exception");
    }

    @GetMapping(value = "/fail2")
    public void helloFail(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "test error response");
    }
}
